#include <gspline/point.hpp>
#include <gspline/hmesh.hpp>

int main() {
    using namespace mmx;
    typedef point<double> Point;
    mmx::hmesh<Point> m;

    m.push_back_vertex(Point(0,0,0));
    m.push_back_vertex(Point(1,0,0));
    m.push_back_vertex(Point(0,1,0));
    m.push_back_vertex(Point(0,0,1));
    int v1 = m.push_back_vertex(Point(1,1,0));
    int v2 = m.push_back_vertex(Point(0,1,1));

    m.push_back_hedges(0,1); // 0 1
    m.push_back_hedges(1,2); // 2 3
    m.push_back_hedges(2,0); // 4 5
    m.push_back_hedges(0,3); // 6 7
    m.push_back_hedges(1,3); // 8 9
    m.push_back_hedges(2,3); // 10 11

    m.push_back_face(0,2,4); // 0 1 2
    m.push_back_face(6,9,1); // 0 3 1
    m.push_back_face(8,11,3); // 1 3 2
    m.push_back_face(10,7,5); // 2 3 0

    m.split_edge(2,v1);
    m.split_face(2,3,4);
    m.split_edge(10,v2);
    m.split_face(4,v1,v2);

    std::cout<<"<axl>\n";
    print_axl(std::cout,m);
    std::cout<<"</axl>"<<std::endl;

    return 0;
}

