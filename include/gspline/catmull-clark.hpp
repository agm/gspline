#ifndef CATMULLCLARK_HPP
#define CATMULLCLARK_HPP
//  Contribution by Zhenyu Xu, Hajar Sedki, Yun Tian, Kun Zhang (MAM4).

/**
 * @brief centre_face
 * @param f
 * @return the index of the barycentre found.
 *
 *  find the new vertex in centre of face f.
 */
template<class MESH>
int centre_face(MESH* m, int f){

  unsigned e0 = m->face(f).edge();
  int p = m->hedge(e0).head();
  unsigned e = m->hedge(e0).next();

  double px = m->vertex(p).x();
  double py = m->vertex(p).y();
  double pz = m->vertex(p).z();

  while (e!=e0) {
    p = m->hedge(e).head();
    px = px+m->vertex(p).x();
    py = py+m->vertex(p).y();
    pz = pz+m->vertex(p).z();
    e = m->hedge(e).next();
  }

  px = 0.25*px;
  py = 0.25*py;
  pz = 0.25*pz;
  int r;
  r = m->push_back_vertex(px,py,pz);
  return r;
}


/**
 * @brief centre_edge
 * @param e   edge
 * @param v0  centre de surface1
 * @param v1  centre de surface2
 * @return the index of the new vertex of edge.
 *
 *  find the edge vertex interior
 */
template<class MESH>
int centre_edge(MESH* m, int e, int v0, int v1){
  int o = m->hedge(e).opp();
  int p0 = m->hedge(e).head();
  int p1 = m->hedge(o).head();

  double px = 0.25*(m->vertex(p0).x() + m->vertex(p1).x() + m->vertex(v0).x() + m->vertex(v1).x());
  double py = 0.25*(m->vertex(p0).y() + m->vertex(p1).y() + m->vertex(v0).y() + m->vertex(v1).y());
  double pz = 0.25*(m->vertex(p0).z() + m->vertex(p1).z() + m->vertex(v0).z() + m->vertex(v1).z());

  int p = m->push_back_vertex(px,py,pz);
  return p;
}

/**
 * @brief centre_edge
 * @param e   edge
 * @return the index of the new vertex of edge.
 *
 *  find the edge vertex on boundary
 */
template<class MESH>
int centre_edge(MESH* m, int e){
  int p1 = m->hedge(e).head();
  int e0 = m->hedge(e).next();
  int e1 = m->hedge(e0).next();
  int e2 = m->hedge(e1).next();
  int p2 = m->hedge(e2).head();
  double px = 0.5*(m->vertex(p1).x() + m->vertex(p2).x());
  double py = 0.5*(m->vertex(p1).y() + m->vertex(p2).y());
  double pz = 0.5*(m->vertex(p1).z() + m->vertex(p2).z());

  int p = m->push_back_vertex(px,py,pz);
  return p;
}




/**
 * @brief Catmull-clarck subdivision scheme.
 *
 * Inplace subidvision using Catmull-Clark subdivision scheme.
 * @param m mesh which is subdivided
 * @param Ns number of subdivisions.
 */
template<class MESH>
void cc_subdivide(MESH* m, unsigned Ns = 1) {
    int nbv, nbh, nbf, n, e, o, f, p, p0, p1, p2, v0, v1, e0 ,e1, f0, f1, j, i, q;
    double a = 0.0;
    double b = 0.0;

    std::map<int,int> cf;  // < indice_face, indice_vertex >
    std::map<int,int>::iterator it_cf;

    std::map<int,int> ce;  // < indice_edge, indice_vertex >
    std::map<int,int>::iterator it_ce;

    std::map<int,int> boundary;  // < indice_vertex_on_boundary, indice_edge_on_boundary >
    std::map<int,int>::iterator it_b;

    for(unsigned iteration=0; iteration<Ns ;iteration++){

        nbv = m->nbv();
        nbh = m->nbh();
        nbf = m->nbf();

        cf.clear();
        ce.clear();
        boundary.clear();

        // Stock the vertex and edge on boundary
        for ( j=0; j<nbh; j++) {
            if (m->hedge(j).opp()==-1) {
                p = m->hedge(j).head();
                boundary[p]=j;
            }
        }

        // Add face point
        for ( j=0; j<nbf; j++) {
            cf[j]=centre_face(m,j);
        }

        // Add edge point
        for ( j=0; j<nbh; j++) {

            o = m->hedge(j).opp();

            f0 = m->hedge(j).face();
            it_cf=cf.find(f0);
            p0 = it_cf->second;

            if (o!=-1) {
                f1 = m->hedge(o).face();
                it_cf=cf.find(f1);
                p1 = it_cf->second;

                it_ce = ce.find(o);
                if (it_ce==ce.end()) {
                    ce[j]=centre_edge(m, j, p0, p1);  // Didn't find
                }
            } else {
                ce[j]=centre_edge(m, j);
            }
        }

        // New vertex point
        for ( j=0; j<nbv; j++) {

            it_b = boundary.find(j);
            if (it_b==boundary.end()) {  // points intérieurs

                n = m->nb_edge_adjacent(j);
                e = m->edge_adjacent(j);
                f = m->hedge(e).face();

                a = (double)(n-2)/n;
                b = (double)1/(n*n);

                m->vertex(j).x()=m->vertex(j).x()*a;
                m->vertex(j).y()=m->vertex(j).y()*a;
                m->vertex(j).z()=m->vertex(j).z()*a;

                for ( i=0; i<n; i++) {
                    it_ce=ce.find(e);

                    if(it_ce==ce.end()) {
                        o = m->hedge(e).opp();
                        it_ce=ce.find(o);
                    }
                    p0 = it_ce->second;

                    it_cf=cf.find(f);
                    p1 = it_cf->second;

                    m->vertex(j).x() += (m->vertex(p0).x()+m->vertex(p1).x())*b;
                    m->vertex(j).y() += (m->vertex(p0).y()+m->vertex(p1).y())*b;
                    m->vertex(j).z() += (m->vertex(p0).z()+m->vertex(p1).z())*b;

                    e = m->hedge(m->hedge(e).next()).opp();
                    f = m->hedge(e).face();
                }
            } else if (it_b!=boundary.end()) {  // point sur bord
                e = it_b->second;
                e0 = e;

                do {
                    e1 = m->hedge(e0).next();
                    e0 = m->hedge(e1).opp();
                } while (m->hedge(e1).opp()!=-1);

                it_ce=ce.find(e);
                p0 = it_ce->second;

                it_ce=ce.find(e1);
                p1 = it_ce->second;

                double mu=0.5, nu=0.25;
                m->vertex(j).x()=mu*m->vertex(j).x()+nu*(m->vertex(p0).x()+m->vertex(p1).x());
                m->vertex(j).y()=mu*m->vertex(j).y()+nu*(m->vertex(p0).y()+m->vertex(p1).y());
                m->vertex(j).z()=mu*m->vertex(j).z()+nu*(m->vertex(p0).z()+m->vertex(p1).z());
            }
        }

        //Split every original face into four small faces
        //Split every original edge into 2
        for ( j=0; j<nbh; j++) {
            it_ce = ce.find(j);
            if(it_ce!=ce.end()) {
                p = it_ce->second;
                m->split_edge(j,p);
            }
        }

        //Split every original face into 2
        for ( j=0; j<nbf; j++) {
            it_cf = cf.find(j);
            p = it_cf->second;

            e = m->face(j).edge();
            p0 = m->hedge(e).head();
            e = m->hedge(m->hedge(e).next()).next();
            v0 = m->hedge(e).head();
            e = m->hedge(m->hedge(e).next()).next();
            p1 = m->hedge(e).head();
            e = m->hedge(m->hedge(e).next()).next();
            v1 = m->hedge(e).head();

            e0 = m->split_face(j, p0, p1);
            //f = m->nbf()-1;
        }

        //Split new edges into 2
        nbh = m->nbh();
        for ( j=0; j<nbf; j++){
            it_cf=cf.find(j);
            p = it_cf->second;
            q = nbh-1-2*nbf+2*j+2;
            m->split_edge(q, p);
        }

        //Split new faces into 2
        nbf = m->nbf();
        for (j=0; j<cf.size(); j++) {
            it_cf = cf.find(j%cf.size());
            p = it_cf->second;

            e = m->face(j).edge();
            do {
                p0 = m->hedge(e).head();
                e = m->hedge(e).next();
            } while (p0 != p);

            o = m->hedge(m->hedge(e).opp()).next();
            o = m->hedge(m->hedge(o).next()).next();
            p2 = m->hedge(o).head();

            e = m->hedge(m->hedge(e).next()).next();
            p1 = m->hedge(e).head();

            m->split_face(j, p0, p1);
            m->split_face(j+cf.size(), p0, p2);

        }
    }
}

#endif // CATMULLCLARK_HPP
