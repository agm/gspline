#pragma once

#include <vector>

#define TMPL template <class VERTEX, class EDGE, class FACE>
#define SELF hmesh<VERTEX,EDGE,FACE>

//--------------------------------------------------------------------
namespace mmx {
//--------------------------------------------------------------------
struct hedge {
    hedge(): m_pt(-1), m_ne(-1), m_opp(-1), m_face(-1) {}

    hedge(int p): m_pt(p), m_ne(-1), m_opp(-1), m_face(-1) {}
    hedge(int p, int e): m_pt(p), m_ne(e), m_opp(-1), m_face(-1) {}
    hedge(int p, int e, int o): m_pt(p), m_ne(e), m_opp(o), m_face(-1) {}
    hedge(int p, int e, int o, int f): m_pt(p), m_ne(e), m_opp(o), m_face(f) {}

    int  head(void) const { return m_pt; }
    int& head(void )      { return m_pt; }

    int  next(void) const { return m_ne; }
    int& next(void )      { return m_ne; }

    int  opp(void) const   { return m_opp; }
    int& opp(void )        { return m_opp; }

    int  face(void) const { return m_face; }
    int& face(void )      { return m_face; }

public:
    int m_pt;
    int m_ne;
    int m_opp;
    int m_face;
};

//--------------------------------------------------------------------
struct hface {
    hface(): m_he(-1) {}
    hface(int e): m_he(e) {}

    int  edge(void) const { return m_he; }
    int& edge(void )      { return m_he; }

public:
    int m_he;
};

//--------------------------------------------------------------------
template <class VERTEX, class EDGE = hedge, class FACE = hface >
struct hmesh {

    typedef VERTEX Vertex;
    typedef EDGE   Edge;
    typedef FACE   Face;

    const Vertex& vertex(int i) const { return m_vertices[i]; }
          Vertex& vertex(int i)       { return m_vertices[i]; }

    int   push_back_vertex(double x, double y, double z)  {
        m_vertices.push_back(Vertex(x,y,z));
        return m_vertices.size()-1;
    }

    int   push_back_vertex(const Vertex &p)  {
        m_vertices.push_back(p);
        return m_vertices.size()-1;
    }

    const Edge& hedge(int i) const { return m_edges[i]; }
          Edge& hedge(int i)       { return m_edges[i]; }

    int next_hedge (int e) const {
        return m_edges[e].next();
    }

    int opp_hedge (int e) const {
        return m_edges[e].opp();
    }

    int push_back_hedge (const Edge& e)  {
        m_edges.push_back(e);
        return m_edges.size()-1;
    }

    int push_back_hedges (int v0, int v1)  {
        int n = m_edges.size();
        m_edges.push_back(Edge(v1,-1,n+1));
        m_edges.push_back(Edge(v0,-1,n));
        return n+1;
    }

    const Face& face(int i) const { return m_faces[i]; }
          Face& face(int i)       { return m_faces[i]; }

    int push_back_face (const Face& f)  {
        m_faces.push_back(f);
        return m_faces.size()-1;
    }

    /**
     * @brief push_back_face
     * @param e0
     * @param e1
     * @param e2
     * @return
     *
     * Create the triangle with edges e0, e1, e2
     */
    int push_back_face (int e0, int e1, int e2)  {
        int f =  m_faces.size();
        m_faces.push_back(Face(e0));
        m_edges[e0].next()=e1; m_edges[e0].face()=f;
        m_edges[e1].next()=e2; m_edges[e1].face()=f;
        m_edges[e2].next()=e0; m_edges[e2].face()=f;
        return f;
    }

    /**
     * @brief push_back_face
     * @param e0
     * @param e1
     * @param e2
     * @param e3
     * @return
     *
     * Create the quadrangle with edges e0, e1, e2, e3
     */
    int push_back_face (int e0, int e1, int e2, int e3)  {
        int f =  m_faces.size();
        m_faces.push_back(Face(e0));
        m_edges[e0].next()=e1; m_edges[e0].face()=f;
        m_edges[e1].next()=e2; m_edges[e1].face()=f;
        m_edges[e2].next()=e3; m_edges[e2].face()=f;
        m_edges[e3].next()=e0; m_edges[e3].face()=f;
        return f;
    }

    /**
     * @brief split_edge
     * @param e0
     * @param v
     *
     * Split the edge e0 by inserting the vertex v.
     */
    void split_edge(int e0, int v) {
        if(this->hedge(e0).head()!=v){
            int e1, o0, o1;
            e1 = push_back_hedge(Edge(this->hedge(e0)));
            this->hedge(e0).head() = v;
            this->hedge(e0).next() = e1;

            if( (o0=this->hedge(e0).opp()) != -1) {

                o1 = this->push_back_hedge(Edge(this->hedge(o0)));

                this->hedge(o0).head() = v;
                this->hedge(o1).next() = this->hedge(o0).next();
                this->hedge(o0).next() = o1;

                this->hedge(e0).opp()  = o1;
                this->hedge(e1).opp()  = o0;

                this->hedge(o0).opp()  = e1;
                this->hedge(o1).opp()  = e0;
            }
        }
    }

    /**
     * @brief split_face
     * @param f
     * @param v0
     * @param v1
     * @return the index of the new created face.
     *
     *  Split the face f by inserting the edge between the vertices v0 and v1.
     */
    int split_face(int f, int v0, int v1) {

        int ef  = this->face(f).edge(); 
        int e=ef;
 
        int n0 = this->nbh(), n1 = n0+1; 

        int e1, e2, v, v2 ;
        do {
            e1 = this->hedge(e).next();
	    v = this->hedge(e).head();
	    e = e1;
        } while (v!=v1 && v!=v0); 

        if (v==v0) { v2=v1; }
        else { v2=v0; }

        e1 = push_back_hedge(Edge(v2));  //e1 belong to the new face
        e2 = push_back_hedge(Edge(v));   //e2 belong to the old face
        this->hedge(e1).opp() = n1; 
        this->hedge(e2).opp() = n0; 

        e=ef;
        v = this->hedge(ef).head();
        //std::cout<<"split "; println(*this,e);

        int cf = f, nf = this->nbf();

        int E[2];
        int N[2];
        int i=0;

        if(v==v0) {
            E[i]=e;
            N[i]=n0;
            //std::cout<<"0.  "<<E[i]<<" "<<N[i]<<std::endl;
            i++;
            std::swap(cf,nf);
        } else if(v==v1) {
            E[i]=e;
            N[i]=n1;
            //std::cout<<"1.  "<<E[i]<<" "<<N[i]<<std::endl;
            i++;
            std::swap(cf,nf);
        }

        while(((e=this->next_hedge(e)) != -1) && (e != ef) && i<2) {
            //std::cout<<" split "; println(*this,e);
            v = this->hedge(e).head();
            this->hedge(e).face()=cf;
            if(v==v0) {
                E[i]=e;
                N[i]=n0;
                //std::cout<<" 0.    "<<E[i]<<" "<<N[i]<<std::endl;
                i++;
                std::swap(cf,nf);
            } else if(v==v1) {
                E[i]=e;
                N[i]=n1;
                //std::cout<<" 1.    "<<E[i]<<" "<<N[i]<<std::endl;
                i++;
                std::swap(cf,nf);
            }
        }

        if(i==2) {

            this->push_back_face(Face(N[1]));

            this->hedge(N[1]).next()=this->hedge(E[0]).next();
            this->hedge(N[0]).next()=this->hedge(E[1]).next();

            this->hedge(E[0]).next()=N[0];
            this->hedge(E[1]).next()=N[1];

            this->hedge(N[0]).face() = cf;
            this->hedge(N[1]).face() = nf;
        }
        return n1;
    }

/*    int split_face(int f, int v0, int v1) {

        int ef  = this->face(f).edge(), e=ef;

        int n0 = this->nbh(), n1 = n0+1;

        this->hedge(n0).opp() = n1;
        this->hedge(n1).opp() = n0;

        int v = this->hedge(e).head();

        //std::cout<<"split "; println(*this,e);

        int cf = f, nf = this->nbf();

        int E[2];
        int N[2];
        int i=0;

        if(v==v0) {
            E[i]=e;
            N[i]=n0;
            //std::cout<<"0.  "<<E[i]<<" "<<N[i]<<std::endl;
            i++;
            std::swap(cf,nf);
        } else if(v==v1) {
            E[i]=e;
            N[i]=n1;
            //std::cout<<"1.  "<<E[i]<<" "<<N[i]<<std::endl;
            i++;
            std::swap(cf,nf);
        }

        while(((e=this->next_hedge(e)) != -1) && (e != ef) && i<2) {
            //std::cout<<" split "; println(*this,e);
            v = this->hedge(e).head();
            this->hedge(e).face()=cf;
            if(v==v0) {
                E[i]=e;
                N[i]=n0;
                //std::cout<<" 0.    "<<E[i]<<" "<<N[i]<<std::endl;
                i++;
                std::swap(cf,nf);
            } else if(v==v1) {
                E[i]=e;
                N[i]=n1;
                //std::cout<<" 1.    "<<E[i]<<" "<<N[i]<<std::endl;
                i++;
                std::swap(cf,nf);
            }
        }

        if(i==2) {

            this->push_back_hedge(Edge(v1));
            this->push_back_hedge(Edge(v0));
            this->hedge(n0).opp() = n1;
            this->hedge(n1).opp() = n0;

            this->push_back_face(Face(N[1]));

            this->hedge(N[1]).next()=this->hedge(E[0]).next();
            this->hedge(N[0]).next()=this->hedge(E[1]).next();

            this->hedge(E[0]).next()=N[0];
            this->hedge(E[1]).next()=N[1];

            this->hedge(N[0]).face() = cf;
            this->hedge(N[1]).face() = nf;
        }
        return n1;
    }
*/

    unsigned nbv() const { return m_vertices.size(); }
    unsigned nbh() const { return m_edges.size(); }
    unsigned nbf() const { return m_faces.size(); }


//************fonction ajoutée***************


    /**
     * @brief nb_edge_adjacent
     * @param v
     * @return the number of adjacent edges.
     *
     *  find the number of edges which head v.
     */
    int nb_edge_adjacent(int v) {
      int n = 0;
      for (unsigned j=0; j<this->nbh(); j++) {
        if (this->hedge(j).head()==v) { n++; }
      }
      return n;
    }


    /**
     * @brief edge_adjacent
     * @param v
     * @return the indice of an adjacent edge.
     *
     *  find an edge which head v.
     */
    int edge_adjacent(int v) {
      int e;
      for (unsigned j=0; j<this->nbh(); j++) {
        if (this->hedge(j).head()==v) { e = j; }
      }
      return e;
    }

//************fonction fini*****************

private:
    std::vector<VERTEX> m_vertices;
    std::vector<EDGE>   m_edges;
    std::vector<FACE>   m_faces;
};

//--------------------------------------------------------------------
TMPL
void println(const SELF& m, int e) {
    std::cout<< "e: "<<e
             <<" n: "<<m.hedge(e).next()
             <<" v: "<<m.hedge(e).head()
             <<" f: "<<m.hedge(e).face()<<std::endl;
}
//--------------------------------------------------------------------
template<class OSTREAM, class VERTEX, class EDGE, class FACE>
void print_axl(OSTREAM & os, const SELF& m){
    os<<"<mesh>\n";
    os<<"<count>"<<m.nbv()<<" 0 "<<m.nbf()<<"</count>\n";
    os<<"<points>\n";
    for(int i=0;i<m.nbv();i++){
        os<<m.vertex(i).x()<<" "<<m.vertex(i).y()<<" "<<m.vertex(i).z()<<"\n";
    }
    os<<"</points>\n";
    if(m.nbf()>0) {
        std::vector<int> f;
        os<<"<faces>\n";
        for(int i=0;i<m.nbf();i++) {
            int e = m.face(i).edge(), e0=e;
            f.resize(0);
            f.push_back(m.hedge(e).head());
            //std::cout<<" m "; println(m,e);
            while(((e=m.next_hedge(e)) != -1) && (e!=e0)) {
                f.push_back(m.hedge(e).head());
                //std::cout<<" m "; println(m,e);
            }
            os<<f.size();
            for(int j=0;j<f.size();j++) os<<" "<<f[j]; os<<"\n";
        }
        os<<"</faces>\n";
    }
    os<<"</mesh>"<<std::endl;
}

//--------------------------------------------------------------------
} // namespace mmx
//--------------------------------------------------------------------
#undef TMPL
#undef SELF
