find_package(Qt5Core REQUIRED)
include_directories(${Qt5Core_INCLUDE_DIRS})
link_libraries(${Qt5Core_LIBRARIES})

find_package(Qt5OpenGL REQUIRED)
include_directories(${Qt5OpenGL_INCLUDE_DIRS})
link_libraries(${Qt5OpenGL_LIBRARIES})

find_package(Qt5Xml REQUIRED)
include_directories(${Qt5Xml_INCLUDE_DIRS})
link_libraries(${Qt5Xml_LIBRARIES})

find_package(Axel)
include_directories(${AXEL_INCLUDE_DIR})
link_directories(${AXEL_LIBRARY_DIR})
link_libraries(${AXEL_LIBRARIES})