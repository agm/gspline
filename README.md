# Package for geometricaly smooth splines on meshes of arbitrary topology

### Configuration

```
mkdir build-gspline
cd build-gspline
cmake ../gspline
```

If you have installed `Axel` with conda (see https://gitlab.inria.fr/axel/axel), you can configure `gspline` with `Axel`  as follows:
```
cmake ../gspline -DAXL=ON
```

If you have built `Axel` from its sources, you can configure `gspline` with `Axel`  as follows:

```
cmake ../gspline -DAXL=ON -DAxel_DIR=/path/to/build-axel
```

where `/path/to/build-axel`is the path to the build folder of Axel. 

### Compilation

```
make
```

### Tests

```
./test/gspline_mesh
make test
```

