//------------------------------------------------------------------------------
// For the plugins 
// #include <axel-config.h>
#include <axlCore/axlAbstractData.h>
#include <axlCore/axlReader.h>
#include <axlCore/axlWriter.h>
#include <axlCore/axlFactoryRegister.h>
#include <dtkCoreSupport/dtkPluginManager.h>

#include <axlCore/axlMesh.h>
#include <gspline/point.hpp>
#include <gspline/hmesh.hpp>
#include <gspline/catmull-clark.hpp>
#include <set>
#include <vector>

//------------------------------------------------------------------------------
int main(int argc, char ** argv) {

    int maxlevel = 4;

    if (argc <2) {
        std::cout<< "usage: "<<argv[0]<< " file.axl"<<std::endl;
        return 1;
    }

    if (argc >2) {
        maxlevel = atoi(argv[2]);
    }
    
    //----------------------------------------------------------------------
    // Axel input
    //----------------------------------------------------------------------
    // Loading Axel plugins
    // dtkPluginManager::instance()->setPath(AXEL_PLUGIN_DIR);
    dtkPluginManager::instance()->initializeApplication();
    dtkPluginManager::instance()->initialize();
    axlFactoryRegister::initialized();

    //Reading input file
    qDebug()<<"Input  file: "<<argv[1];
    axlReader *obj_reader = new axlReader();
    obj_reader->read(QString(argv[1]));
    QList<axlAbstractData *> list = obj_reader->dataSet();

    // Print description of data0.
    //if(list.size() >0) qDebug()<<list.at(0)->description();
    axlMesh* m0 = dynamic_cast<axlMesh*>(list.at(0));

    //----------------------------------------------------------------------
    // mesh with only the edges of m0
    axlMesh* m1 = new axlMesh;
    
    for (unsigned i=0; i<m0->vertex_count(); i++) {
        m1->push_back_vertex(m0->vertexX(i), m0->vertexY(i), m0->vertexZ(i));
    }

    unsigned s;
    for (unsigned i=0; i<m0->face_count(); i++) {
        s = m0->face(i).size();
        for (unsigned j=0;j<s;j++)
            m1->push_back_edge(m0->face(i)[j], m0->face(i)[(j+1)%s]);
    }

    m1->setColor(QColor(0,0,255));
    m1->setSize(0.6);

    //----------------------------------------------------------------------
    // Construction of a hmesh
    //----------------------------------------------------------------------
    typedef mmx::point<double> Point;
    typedef mmx::hmesh<Point>  Mesh;
    
    typedef mmx::hedge         Edge;
    typedef mmx::hface         Face;

    Mesh* msh = new Mesh;
    Mesh* m = new Mesh;

    for (unsigned i=0; i<m0->vertex_count(); i++) {
        msh->push_back_vertex(m0->vertexX(i), m0->vertexY(i), m0->vertexZ(i));
    }

    int i0, i1, opp, nxt, ne;
    typedef std::pair<int,int> Idx;
    std::map<Idx,int> I;
    std::map<Idx,int>::iterator it;

    for (unsigned i=0; i<m0->face_count(); i++) {
        s  = m0->face(i).size();
        ne = msh->nbh();

        for (unsigned j=0;j<s;j++) {
            i0 = m0->face(i)[j];

            i1 = m0->face(i)[(j+1)%s];

            I[Idx(i0,i1)] = msh->nbh();

            nxt=ne+((j+1)%s);
            opp = -1;

            // Check if the opposite edge exists
            it = I.find(Idx(i1,i0));
            if(it != I.end()) {
                opp = it->second;
                msh->hedge(opp).opp() = ne+j;
            }
            Edge e(i1, nxt, opp, i);
            msh->push_back_hedge(e);
        }
        Face f(ne);
        msh->push_back_face(f);
    }
    //int nu=msh->face(1).edge();
    //std::cout<<"    "<<nu<<"   "<<msh->hedge(nu).head()<<std::endl;

    //----------------------------------------------------------------------
    // Subdivision/
    //----------------------------------------------------------------------

    m=msh;



    //    for(iteration=0;iteration<1;iteration++){

    cc_subdivide(m, maxlevel);

    msh = m;

    //convert msh to mn
    axlMesh* mn = new axlMesh;

    for (unsigned i=0; i<msh->nbv(); i++) {
        mn->push_back_vertex(msh->vertex(i).x(), msh->vertex(i).y(), msh->vertex(i).z());
    }

    int p1, p2, v0, v1, e0 ,e1, f0, f1;
    axlMesh::Face F;
    for (unsigned i=0; i<msh->nbf(); i++) {
        s = msh->face(i).edge();
        e0 = s;
        F.resize(0);
        do {
            p1 = msh->hedge(e0).head();
            F.push_back(p1);
            e1 = msh->hedge(e0).next();
            p2 = msh->hedge(e1).head();
            //mn->push_back_edge(p1, p2);
            e0 = e1;
        } while (s!=e1);
        mn->push_back_face(F);
    }

    mn->setColor(QColor(0,0,255));
    mn->setSize(0.1);

    //----------------------------------------------------------------------
    // Axel output
    //----------------------------------------------------------------------
    //Writing output file
    axlWriter *obj_writer = new axlWriter();
    // All the data of the file input.axl
    //    foreach(axlAbstractData* o, list) obj_writer->addDataToWrite(o);
    //obj_writer->addDataToWrite(m0);
    //obj_writer->addDataToWrite(m1);

    //Writing result of subdivision
    obj_writer->addDataToWrite(mn);

    obj_writer->write("output.axl");
    qDebug()<<"Output file: output.axl";
    system("axel output.axl&");
    //mmx::print_axl(std::cout,*msh);//*
}

//------------------------------------------------------------------------------		
